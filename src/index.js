// Fonts
import(/* webpackPrefetch: true */ './assets/fonts/Bebas.css');
import(/* webpackPrefetch: true */ './assets/fonts/Montserrat.css');

// Stylesheets
import(/* webpackPrefetch: true */ './assets/css/custom.css');
import(/* webpackPrefetch: true */ './assets/css/bootstrap.css');
import '@fortawesome/fontawesome-free/css/all.min.css';

// Javascript
import 'bootstrap';
import { Collapse, ScrollSpy } from 'bootstrap';


// check if month is in fishing season, if so, update the call to action text
const checkMonth = () => {
    const today = new Date();
    const month = today.getMonth() + 1;
    const text = month > 4 ? "in full swing" : "almost here";
    const elm = document.getElementById("seasonTxt");
    if (elm) elm.innerHTML = text;
};

// update the current year
const updateYear = () => {
    const year = new Date().getFullYear();
    const elm = document.getElementById("fishingYear");
    if (elm) elm.innerHTML = year;
    const elm2 = document.getElementById("cYear");
    if (elm2) elm2.innerHTML = year;
}

// fade out the loader when page is loaded
const fadeOutEffect = () => {
    const fadeTarget = document.getElementById("pageLoader");
    const fadeEffect = setInterval(() => {
        if (!fadeTarget.style.opacity) {
            fadeTarget.style.visibility = "visible";
            fadeTarget.style.opacity = 1;
        }
        if (fadeTarget.style.opacity > 0) {
            fadeTarget.style.opacity -= 0.1;
        } else {
            document.getElementById("pageLoader").style.visibility = "hidden";
            clearInterval(fadeEffect);
            // after loader is faded out, update text and fade in content
            updateYear();
            checkMonth();
            fadeInEffect();
        }
    }, 200);
};

// fade in the content of the webpage
const fadeInEffect = () => {
    const fadeTarget = document.getElementById("wrapper");
    fadeTarget.style.opacity = 0;
    fadeTarget.style.visibility = "visible";
    let opacity = 0;
    const inEffect = setInterval(() => {
        if (opacity < 1) {
            opacity += 0.1;
            fadeTarget.style.opacity = opacity;
        } else {
            clearInterval(inEffect);
            // check navbar and refresh scrollspy when content is fully visible
            checkNavBar();
            scroll();
        }
    }, 200);
};

// listen to navbar toggle clicks
const checkNavBar = () => {
    // when open, make nav full screen and disable background scroll
    const navbar = document.getElementById('navbarBtn');
    navbar.addEventListener('click', (event) => {
        setTimeout(() => {
            const nav = document.getElementById('navbarNav');
            nav.classList.contains('show') ? document.body.classList.add('lock-scroll') : document.body.classList.remove('lock-scroll');
        }, 500);

    });
    // when navbar link is clicked in full screen menu, make sure menu closes
    const navLinks = document.querySelectorAll('.nav-item');
    const nav = document.getElementById('navbarNav');
    const bsCollapse = new Collapse(nav, { toggle: false });
    navLinks.forEach((link) => {
        link.addEventListener('click', () => {
            if (window.innerWidth < 992) bsCollapse.toggle();
            document.body.classList.remove('lock-scroll');
        });
    });
};

// refresh navbar scrollspy, so that the active nav element will be highlighted on scroll
const scroll = () => {
    const firstScrollSpyEl = document.querySelector('[data-bs-spy="scroll"]');
    ScrollSpy.getInstance(firstScrollSpyEl).refresh();
};

// fade out loader once page is loaded
window.addEventListener("DOMContentLoaded", () => {
    fadeOutEffect();
});

// if window is resized while collapsed navbar is open, close the navbar
window.onresize = () => {
    const nav = document.getElementById('navbarNav');
    if (window.innerWidth >= 991 && nav.classList.contains('show')) {
        const bsCollapse = new Collapse(nav, { toggle: true });
        bsCollapse.toggle();
        document.body.classList.remove('lock-scroll');
    }
};
